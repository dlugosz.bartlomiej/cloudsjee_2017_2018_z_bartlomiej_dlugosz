#!/bin/bash

echo "------- shell apt -------"


apt-get remove --purge openjdk-7-jdk openjdk-7-jre openjdk-7-jre-headless -y
add-apt-repository ppa:openjdk-r/ppa -y

apt-get update
apt-get install -y openjdk-8-jdk rsync dsh htop git maven

sudo apt-get remove --purge openjdk-7-jdk openjdk-7-jre openjdk-7-jre-headless -y
