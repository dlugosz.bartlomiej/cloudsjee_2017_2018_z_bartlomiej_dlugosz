package pl.bdlugosz.chmury.ignite.controller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class LoginController {

    private static final String OUTPUT_DIRECTORY = "/home/vagrant/output";

    @RequestMapping(value = {"login"})
    public String login() {

        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (!(auth instanceof AnonymousAuthenticationToken)) {
                return "secret";
            }
            return "login";
        } catch (Exception e) {
            return "redirect:/error/500";
        }
    }

    @RequestMapping(value = {"/home", "/"})
    public String difult() {
        return "home";
    }

    @RequestMapping(value = "/secret")
    public String home(Model model) throws IOException {
        File dir = new File(OUTPUT_DIRECTORY);
        FileFilter fileFilter = new WildcardFileFilter("part*");
        File[] files = dir.listFiles(fileFilter);

        List<String> lines = new ArrayList();

        try {
            lines = FileUtils.readLines(files[0], "UTF-8");
        } catch (final NullPointerException ex) {

        }
        model.addAttribute("lines", lines);

        return "secret";
    }
}
